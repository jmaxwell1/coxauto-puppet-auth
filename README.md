# auth

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with auth](#setup)
    * [What auth affects](#what-auth-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with auth](#beginning-with-auth)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

This module configures SSSD for RHEL 6 and greater systems
and manages /etc/sudoers

## Module Description

Implements host access and ldap servers in sssd.conf and
manages /etc/sudoers


### What auth affects

- SSSD.conf and the required packages
- Sudoers

## Limitations

Only compatible with RHEL 6 and greater

## Development

This one's super simple, let's see some merge requests!

## Release Notes/Contributors/Etc
2015.10.22 - Jamison - Added sudoers management to this mod

2015.10.19 - Jamison - Created module based on TTucker's work on the sssd module
