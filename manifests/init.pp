#SSSD config
class coxauto-auth {

  $OS =  $::operatingsystem
  $osrelease = $::operatingsystemmajrelease
  $ssdpkgs = [
    'sssd',
    'sssd-krb5',
    'sssd-ldap',
    'sssd-common',
    'sssd-ad',
    'sssd-client',
  ]

  file { '/etc/sudoers':
    source  => 'puppet:///modules/coxauto-auth/etc/sudoers',
    owner   => 'root',
    group   => 'root',
    mode    => '0440'
    require => Package['sudo'],
  }

  if $OS == 'RedHat' {
    if $osrelease >= '6' {
      
      package { $ssdpkgs:
        ensure          => 'installed',
        install_options => '--no_gpg_check',
      }

        service { 'sssd':
          ensure  => 'running',
          enable  => true,
          require => File['/etc/sssd/sssd.conf'],
        }

        file { '/etc/sssd/sssd.conf':
          content => template('coxauto-sssd/sssd.conf.erb'),
          owner   => 'root',
          group   => 'root',
          mode    => '0600',
          notify  => Service['sssd'],
        }
      }
    }
}
